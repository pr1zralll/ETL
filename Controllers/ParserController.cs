using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ETL.Context;
using ETL.Models;
using ETL.Models.Entities;
using ETL.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ETL.Controllers
{
    public class ParserController : Controller
    {
        private readonly MainDbContext _context;

        public ParserController(MainDbContext context)
        {
            _context = context;
        }

        // GET: Parser
        public async Task<IActionResult> Index()
        {
            return View(await _context.Dlls.AsNoTracking().Select(x => new DLLFastViewModel() { Id = x.Id, Name = x.Name, Url = x.Url }).ToListAsync());
        }

        // GET: Parser/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dLL = await _context.Dlls.AsNoTracking()
                .SingleOrDefaultAsync(m => m.Id == id);
            if (dLL == null)
            {
                return NotFound();
            }

            return View(dLL);
        }

        // GET: Parser/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Parser/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Url,File")] DLLCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _context.Dlls.Add(viewModel.ToDLL());
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        // GET: Parser/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _context.Dlls.AsNoTracking().Select(x => new DLLFastViewModel() { Id = x.Id, Name = x.Name, Url = x.Url }).SingleOrDefaultAsync(m => m.Id == id);
            if (viewModel == null)
            {
                return NotFound();
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Url")] DLLFastViewModel viewModel, IFormFile upload)
        {
            if (id != viewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    DLL dll = new DLL() { Name = viewModel.Name, Url = viewModel.Url, Id = viewModel.Id };
                    if (upload == null)
                    {
                        var model = await _context.Dlls.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);
                        dll.File = model.File;
                    }
                    else
                    {
                        using(var sr = new BinaryReader(upload.OpenReadStream()))
                        {
                            dll.File = sr.ReadBytes((int) upload.Length);
                        }
                    }
                    _context.Dlls.Update(dll);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DLLExists(viewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        // GET: Parser/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _context.Dlls.AsNoTracking().Select(x => new DLLFastViewModel() { Id = x.Id, Name = x.Name, Url = x.Url }).SingleOrDefaultAsync(m => m.Id == id);
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        // POST: Parser/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dLL = await _context.Dlls.SingleOrDefaultAsync(m => m.Id == id);
            _context.Dlls.Remove(dLL);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DLLExists(int? id)
        {
            return _context.Dlls.AsNoTracking().Any(e => e.Id == id);
        }
    }
}