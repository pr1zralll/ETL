using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ETL.Context;
using ETL.Models.Entities;
using ETL.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace ETL.Controllers
{
    public class ModelController : Controller
    {
        private readonly MainDbContext _context;

        public ModelController(MainDbContext context)
        {
            _context = context;
        }

        // GET: Model
        public async Task<IActionResult> Index(string where, string column, bool orderDesc = false, string orderby="Id")
        {
            
            IQueryable<DataModel> models = _context.Models;

            //where
            if (where != null)
            {
                if (column == "PubDate")
                    models = models.Where(x => ((DateTime)typeof(DataModel).GetProperty(column).GetValue(x)).Date.ToShortDateString() == where);
                else if (column == "Date")
                    models = models.Where(x => x.Date.ToString("dd.mm.yyyy HH:mm:ss") == where);
                else
                    models = models.Where(x => (string)typeof(DataModel).GetProperty(column).GetValue(x) == where);
                ViewBag.where = where;
                ViewBag.column = column;
            }
           

            //order
            if (orderDesc)
                models = models.OrderByDescending(x => typeof(DataModel).GetProperty(orderby).GetValue(x));
            else
                models = models.OrderBy(x => typeof(DataModel).GetProperty(orderby).GetValue(x));
            ViewBag.orderby = orderby;
            ViewBag.orderDesc = orderDesc;



            return View(await models.AsNoTracking().ToListAsync());
        }

        // GET: Model/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataModel = await _context.Models.Include(x=>x.RawData)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (dataModel == null)
            {
                return NotFound();
            }

            return View(dataModel);
        }

        // GET: Model/Create
        public IActionResult Create()
        {
            ViewData["DLLId"] = new SelectList(_context.Dlls, "Id", "Name");
            DataModelViewModel dt = new DataModelViewModel();
            dt.PubDate = DateTime.Now;
            return View(dt);
        }

        // POST: Model/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DataModelViewModel dataModelViewModel)
        {
            if (dataModelViewModel.PubDate == null)
                dataModelViewModel.PubDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Models.Add(dataModelViewModel.ToDataModel());
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dataModelViewModel);
        }

        // GET: Model/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataModel = await _context.Models.AsNoTracking().Include(x => x.RawData).SingleOrDefaultAsync(m => m.Id == id);
            if (dataModel == null)
            {
                return NotFound();
            }
            return View(dataModel.ToViewModel());
        }

        // POST: Model/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Category,Link,Description,Author,PubDate,Image,RawData,DLLId")] DataModelViewModel dataModelViewModel)
        {
            if (id != dataModelViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var img = _context.Models.AsNoTracking().Include(x => x.Image).SingleOrDefault(x => x.Id == id).Image;
                    _context.Models.Update(dataModelViewModel.ToDataModel(img));
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {

                }
                return RedirectToAction(nameof(Index));
            }
            return View(dataModelViewModel);
        }

        // GET: Model/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var dataModel = await _context.Models.SingleOrDefaultAsync(m => m.Id == id);
            _context.Models.Remove(dataModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}