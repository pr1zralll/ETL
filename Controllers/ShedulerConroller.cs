using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ETL.Context;
using ETL.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace ETL.Controllers
{
    public class ShedulerController : Controller
    {
        private readonly MainDbContext _context;
        public ShedulerController(MainDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Exec(int id)
        {
            var task = await _context.Tasks.Include(x => x.Parser).SingleAsync(x => x.Id == id);
            task.Enabled = true;
            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
            var t = HttpContext.RequestServices.GetService(typeof(IMyService)) as TasksService;
            t.StartAsync();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Kill(int id)
        {
            var task = await _context.Tasks.SingleAsync(x => x.Id == id);
            task.Enabled = false;
            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
            var t = HttpContext.RequestServices.GetService(typeof(IMyService)) as TasksService;
            t.StartAsync();
            return RedirectToAction("Index");
        }
        // GET: Sheduler
        public async Task<IActionResult> Index()
        {
            var mainDbContext = _context.Tasks.Include(p => p.Parser);
            return View(await mainDbContext.ToListAsync());
        }

        // GET: Sheduler/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planedTask = await _context.Tasks
                .Include(p => p.Parser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (planedTask == null)
            {
                return NotFound();
            }

            return View(planedTask);
        }

        // GET: Sheduler/Create
        public IActionResult Create()
        {
            ViewData["ParserId"] = new SelectList(_context.Dlls, "Id", "Name");

            PlanedTask t = new PlanedTask();
            t.StartTime = DateTime.Now;
            t.Enabled = true;
            t.Status = "done";
            t.Period = DateTime.MinValue.AddMinutes(5);

            return View(t);
        }

        // POST: Sheduler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PlanedTask planedTask)
        {

            if (ModelState.IsValid)
            {
                planedTask.Status = "done";
                _context.Add(planedTask);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(planedTask);
        }

        // GET: Sheduler/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planedTask = await _context.Tasks.SingleOrDefaultAsync(m => m.Id == id);
            if (planedTask == null)
            {
                return NotFound();
            }
            ViewData["ParserId"] = new SelectList(_context.Dlls, "Id", "Name", planedTask.ParserId);
            return View(planedTask);
        }

        // POST: Sheduler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PlanedTask planedTask)
        {
            if (id != planedTask.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(planedTask);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlanedTaskExists(planedTask.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParserId"] = new SelectList(_context.Dlls, "Id", "Name", planedTask.ParserId);
            return View(planedTask);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var planedTask = await _context.Tasks.SingleOrDefaultAsync(m => m.Id == id);
            _context.Tasks.Remove(planedTask);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlanedTaskExists(int id)
        {
            return _context.Tasks.Any(e => e.Id == id);
        }
    }
}