
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ETL.Models.Entities;
using Microsoft.AspNetCore.Http;

namespace ETL.Models.ViewModels
{
    public class DLLCreateViewModel {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [DataType (DataType.Upload)]
        public IFormFile File { get; set; }
        [Required]
        [DataType (DataType.Url)]
        public string Url { get; set; }

        public DLL ToDLL () {
            var dll = new DLL ();
            if (this.Id != null)
                dll.Id = (int) this.Id;
            dll.Name = this.Name;
            dll.Url = this.Url;
            using (var sr = new BinaryReader (this.File.OpenReadStream ())) {
                dll.File = sr.ReadBytes ((int) this.File.Length);
            }
            return dll;
        }
    }
}