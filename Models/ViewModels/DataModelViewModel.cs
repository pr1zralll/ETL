using System;
using System.IO;
using ETL.Models.Entities;
using Microsoft.AspNetCore.Http;

namespace ETL.Models.ViewModels
{
    public class DataModelViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime PubDate { get; set; }

        public IFormFile Image { get; set; }

        public string RawData { get; set; }

        public string Parser { get; set; }
        public string Source { get; set; }

        internal DataModel ToDataModel()
        {
            DataModel dataModel = new DataModel();
            dataModel.Id = this.Id;
            dataModel.Author = this.Author;
            dataModel.Category = this.Category;
            dataModel.Date = DateTime.Now;
            dataModel.Description = this.Description;
            dataModel.Parser = this.Parser;
            dataModel.Link = this.Link;
            dataModel.PubDate = this.PubDate;
            dataModel.Title = this.Title;
            dataModel.Source = this.Source;
            dataModel.Image = new Image();

            using(var sr = new BinaryReader(this.Image.OpenReadStream()))
            {
                dataModel.Image.File = sr.ReadBytes((int) this.Image.Length);
                dataModel.Image.Name = this.Image.FileName;
            }

            dataModel.RawData = new RawData();
            dataModel.RawData.Html = this.RawData;

            return dataModel;
        }
        internal DataModel ToDataModel(Image img)
        {
            DataModel dataModel = new DataModel();
            dataModel.Id = this.Id;
            dataModel.Author = this.Author;
            dataModel.Category = this.Category;
            dataModel.Source = this.Source;
            dataModel.Date = DateTime.Now;
            dataModel.Description = this.Description;
            dataModel.Parser = this.Parser;
            dataModel.Link = this.Link;
            dataModel.PubDate = this.PubDate;
            dataModel.Title = this.Title;

            dataModel.Image = new Image();
            if (this.Image == null)
                dataModel.Image = img;
            else
                using(var sr = new BinaryReader(this.Image.OpenReadStream()))
                {
                    dataModel.Image.File = sr.ReadBytes((int) this.Image.Length);
                    dataModel.Image.Name = this.Image.FileName;
                }

            dataModel.RawData = new RawData();
            dataModel.RawData.Html = this.RawData;

            return dataModel;
        }
    }
}