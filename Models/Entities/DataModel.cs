using System;
using ETL.Models.ViewModels;

namespace ETL.Models.Entities
{
    public class DataModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime PubDate { get; set; }

        public int ImageId { get; set; }
        public Image Image { get; set; }

        public int RawDataId { get; set; }
        public RawData RawData { get; set; }

        public string Parser { get; set; }

        public DateTime Date { get; set; }
        public string Source { get; set; }

        internal DataModelViewModel ToViewModel()
        {
            DataModelViewModel vdm = new DataModelViewModel();
            vdm.Id = this.Id;
            vdm.Author = this.Author;
            vdm.Category = this.Category;
            vdm.Description = this.Description;
            vdm.Parser = this.Parser;
            vdm.Link = this.Link;
            vdm.PubDate = this.PubDate;
            vdm.Title = this.Title;
            vdm.RawData = this.RawData.Html;
            vdm.Source = this.Source;
            return vdm;
        }
    }
}