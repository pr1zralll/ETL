namespace ETL.Models.Entities
{
    public class RawData
    {
        public int Id { get; set; }
        public string Html { get; set; }
    }
}