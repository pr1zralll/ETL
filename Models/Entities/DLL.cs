using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ETL.Models.Entities
{
    public class DLL
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Url)]
        public string Url { get; set; }
        [Required]
        [DataType(DataType.Upload)]
        [Display(Name = "dll")]
        public byte[] File { get; set; }
    }
}