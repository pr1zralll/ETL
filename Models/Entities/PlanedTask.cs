using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ETL.Models.Entities
{
    public class PlanedTask
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Parser")]
        public int ParserId { get; set; }
        public DLL Parser { get; set; }

        [Required]
        [DataType(DataType.Url)]
        public string Url { get; set; }

        public string Status { get; set; }
        [Display(Name = "Done:")]
        public int Complited { get; set; }
        [Display(Name = "Error:")]
        public int ErrorCount { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime? Period { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? StartTime { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? LastUpdate { get; set; }
        public bool Enabled { get; set; }
    }
}