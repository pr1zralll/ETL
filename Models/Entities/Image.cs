using System.ComponentModel.DataAnnotations;

namespace ETL.Models.Entities
{
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }
    }
}