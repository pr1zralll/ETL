﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ETL.Migrations
{
    public partial class add_ModelsContr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Models_Dlls_DLLId",
                table: "Models");

            migrationBuilder.DropIndex(
                name: "IX_Models_DLLId",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "DLLId",
                table: "Models");

            migrationBuilder.AddColumn<string>(
                name: "Parser",
                table: "Models",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "Models",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Parser",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "Source",
                table: "Models");

            migrationBuilder.AddColumn<int>(
                name: "DLLId",
                table: "Models",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Models_DLLId",
                table: "Models",
                column: "DLLId");

            migrationBuilder.AddForeignKey(
                name: "FK_Models_Dlls_DLLId",
                table: "Models",
                column: "DLLId",
                principalTable: "Dlls",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
