using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ETL.Models;
using ETL.Models.Entities;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using ETL.Models.ViewModels;

namespace ETL.Context
{
    public class MainDbContext : DbContext
    {

        public MainDbContext(DbContextOptions options) : base(options) { }

        public DbSet<DLL> Dlls { get; set; }
        public DbSet<PlanedTask> Tasks { get; set; }
        public DbSet<DataModel> Models { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<RawData> Raws { get; set; }
    }
}