using System.Collections.Generic;

namespace ETL.Context
{
    public interface IParser
    {
        List<object> Run(string url);
    }
}