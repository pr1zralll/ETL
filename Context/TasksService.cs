using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ETL.Context;
using ETL.Models;
using ETL.Models.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ETL.Context
{
    public class TasksService : IMyService
    {
        private string conn;

        private int version = 0;

        public TasksService(IConfiguration c)
        {
            Console.WriteLine("shd init");
            conn = c.GetConnectionString("local");
            StartAsync();
        }

        public void StartAsync()
        {
            try
            {
                version++;
                Console.WriteLine("shd  start async");
                Task.Factory.StartNew(Start);
            }
            catch (Exception e)
            {
                Console.WriteLine("my sheduler error");
                Console.WriteLine(e.Message);
            }
        }

        private void Start()
        {
            Console.WriteLine("shd start");
            var bg = new Task(main);

            bg.Start();
        }

        private List<PlanedTask> GetTasks()
        {
            using(MainDbContext db = new MainDbContext(new DbContextOptionsBuilder().UseSqlServer(conn).Options))
            {
                return db.Tasks.AsNoTracking().Include(x => x.Parser).Where(x => x.Enabled == true).ToList();
            }
        }
        private void main()
        {
            var last = version;
            var tasks = GetTasks();
            if (tasks == null || tasks.Count < 1)
            {
                Console.WriteLine("no tasks return");
                return;
            }
            var job = tasks.Where(t => t.StartTime <= DateTime.Now && t.Status.Equals("done")).ToList();
            foreach (var item in job)
            {
                Task.Factory.StartNew(() => Execute(item));
            }
            var min = tasks.Min(t => t.StartTime);
            var time = DateTime.Now.Subtract(min.Value);
            int wait = Convert.ToInt32(new TimeSpan(Math.Abs(time.Ticks)).TotalMilliseconds);

            if (wait < 5000)
                wait = 5000;

            Console.WriteLine("shd sleep for " + wait + " " + new DateTime(Math.Abs(time.Ticks)));

      
                Thread.Sleep(wait);
                if (version == last)
                    StartAsync();
      
        }
        private void Execute(PlanedTask task)
        {
            Console.WriteLine(task.Id + " task started");
            task.Status = "Executing";
            task.StartTime = DateTime.Now.Add(task.Period.Value.TimeOfDay);
            DbUpdate(task);

           // Thread.Sleep(10000);

            List<object> strs = null;
            try
            {
                strs = Parse(task.Parser,task.Url);
            }
            catch (Exception e)
            {
                task.ErrorCount++;
                Console.WriteLine(e.Message);
            }
            
            insertModelsToDB(strs);

            task.Complited++;
            task.Status = "done";
            task.LastUpdate = DateTime.Now;
            DbUpdate(task);
            Console.WriteLine(task.Id + " task end");
        }

        private void insertModelsToDB(List<object> strs)
        {
            if (strs == null || strs.Count < 1)
                return;
            using(MainDbContext db = new MainDbContext(new DbContextOptionsBuilder().UseSqlServer(conn).Options))
            {
                foreach (DataModel dm in strs)
                {
                    db.Models.Add(dm);
                    db.SaveChanges();
                }
            }
        }

        private List<object> Parse(DLL dll,string url)
        {
            var asm = Assembly.Load(dll.File);
            var type = asm.GetTypes().SingleOrDefault(t => t.Name == "Program");
            dynamic c = Activator.CreateInstance(type);
            return c.Run(url);
        }

        private void DbUpdate(PlanedTask task)
        {
            using(MainDbContext db = new MainDbContext(new DbContextOptionsBuilder().UseSqlServer(conn).Options))
            {
                db.Tasks.Update(task);
                db.SaveChanges();
            }
        }
    }
}